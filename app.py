import streamlit as st
from streamlit_chat import message
from streamlit_extras.colored_header import colored_header
from streamlit_extras.add_vertical_space import add_vertical_space
import requests
import dotenv
import openai

dotenv.load_dotenv()
ENV = dotenv.dotenv_values()
openai.api_key = ENV['OPENAI_SECRET_KEY']

st.set_page_config(page_title="Streamlit chat app - to continue Qary bot's conversations")


all_messages = [
        {"role": "system", "content": "You are a helpful assistant."},
        {"role": "assistant", "content": "Hi, I'm the Rutan chatbot at Fondation Botnar. We want to promote gender equity for urban youth around the world. Describe your philanthropy project idea in your own words."},
        {"role": "user", "content": "I would like to start a youth training center for young girls in math."},
        {"role": "assistant", "content": "Do you have any microgrant project ideas?"},
        {"role": "user", "content": "Excellent!"},
        {"role": "assistant", "content": "This combines two of our focus areas - education and gender equity."},
        {"role": "user", "content": "What are your other focus areas?"},
        {"role": "assistant", "content": "We have three focus areas - gender equality, education, and youth. Where are you located?"},
        {"role": "user", "content": "Kenya"},
        {"role": "assistant", "content": "You can find addition information about education in East Africa by visiting this link. https://fondationbotnar.org"},

    ]

# all_messages = [
#         {"role": "system", "content": "You are a helpful assistant."},

#     ]



with st.sidebar:
    st.title('Streamlit Chat App')
    st.markdown('''
    ## About
    This app is an LLM-powered chatbot built using:
    - Qary user prompts
    - Extractive QA model
    - Streamlit
    - GPT-3.5
    
    ''')
    add_vertical_space(5)


if 'generated' not in st.session_state:
    # st.session_state['generated'] = ["Hi, I'm the Rutan chatbot at Fondation Botnar. We want to promote gender equity for urban youth around the world. Describe your philanthropy project idea in your own words.", "Excellent! This combines two of our focus areas - education and gender equity.", "We have three focus areas - gender equality, education, and youth. Where are you located?", "You can find addition information about education in East Africa by visiting this link. https://fondationbotnar.org"]
    st.session_state['generated'] = ["Hi! How can I assist you today?"]
if 'past' not in st.session_state:
    # st.session_state['past'] = ["Hi!", "I would like to start a youth training center for young girls in math.", "What are your other focus areas?", "Kenya"]
    st.session_state['past'] = ["Hi!"]

response_container = st.container()

if 'last_input' not in st.session_state:
    st.session_state['last_input'] = ""


def generate_response(user_statement):
    global all_messages

    all_messages.append({"role": "user", "content":user_statement})

    if "??" in user_statement:
        output = requests.get("https://fastapi-proposal-qa.onrender.com/extract_qa?question=" + user_statement)
        bot_response = output.json()['answer']

    else:
        output = openai.ChatCompletion.create(
            model="gpt-3.5-turbo",
            messages= all_messages
        )
        bot_response = output["choices"][0]["message"]["content"]


    all_messages.append({"role": "assistant", "content":bot_response})

    return bot_response

with response_container:
    with st.spinner('Loading'):
        time.sleep(0.5)
        if st.session_state['last_input']!= "" and st.session_state['last_input']!= st.session_state['past'][len(st.session_state['past'])-1]:
            response = generate_response(st.session_state['last_input'])
            st.session_state.past.append(st.session_state['last_input'])
            st.session_state.generated.append(response)
            
        if st.session_state['generated']:
            for i in range(len(st.session_state['generated'])): 
                message(st.session_state['past'][i], is_user=True, key=str(i) + '_user', avatar_style="micah/svg?seed=ax")#af ax
                message(st.session_state["generated"][i], key=str(i), avatar_style="initials/svg?seed=B​/al")


colored_header(label='', description='', color_name='blue-30')

st.session_state['last_input'] = st.text_input("Your message:", key="text")    

css_example = '''                                                                                                    
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
<i class="fa-solid fa-paper-plane"></i>
'''

if st.button("Send!"):
    st.experimental_rerun()
    


# st.write(css_example, unsafe_allow_html=True)

